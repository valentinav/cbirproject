IM = brandPreprocessing();

functionname='brandPreprocessing.m';
functiondir=which(functionname);
functiondir=functiondir(1:end-length(functionname));
addpath([functiondir '/DATASET'])

path = [functiondir 'DATASET/ROI/'];

%path = 'C:\Users\valen\Documents\2020.1\CBIR\APLICACION\CBIR\DATASET\ROI\';
imag = imread([path 'imagen141.jpg']);
queryImage = processingQueryImage(imag);
dataset = SimilarityMetrics(queryImage, IM);

for i=1:length(dataset)
[values(i),index(i)] = sort([dataset{i}.minDistance],'ascend');
end
[values, index] = sort(values);

for i = 1:6
distanc{i}=dataset{index(i)}.minDistance;
figure,imshow(strcat(path, IM{index(i)}.name));
end
%path2 = 'C:\Users\valen\Documents\2020.1\CBIR\APLICACION\CBIR\DATASET\ORIGINALES\';
path2 = [functiondir 'DATASET/ORIGINALES/'];

arrayD{1} = imread(strcat(path2, IM{index(1)}.name));
imageSize = size(arrayD{1});
numrows = imageSize(1);
numcols = imageSize(2);
for i = 2:12
arrayD{i} = imread(strcat(path2, IM{index(i)}.name));
arrayD{i} = imresize(arrayD{i},[numrows numcols]);
end
montage(arrayD);
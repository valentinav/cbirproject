function IM = SimilarityMetrics(queryImage, IM)
    %data = struct;
    for i = 1:length(IM)
        Ipoits = queryImage.Ipoits;
        D1 = queryImage.D1;
        Ipts2 = IM{i}.Ipts2;
        D2 = IM{i}.D2;
        
        data{i}.vector = matchPoints(Ipoits, D1, Ipts2, D2);
        data{i}.promKP =  1/length(data{i}.vector)*sum(data{i}.vector);
        data{i}.euclideanDist = pdist2(queryImage.normVecGLCM, IM{i}.normaVecGLCM,'euclidean');
    end   
    for i = 1:length(IM)
        IM{i}.minDistance = (0.5*data{i}.promKP) + (0.5*data{i}.euclideanDist);
    end
end

function err = matchPoints(Ipoits, D1, Ipts2, D2)
    err = zeros(1,length(Ipoits));
    cor2 = zeros(1,length(Ipoits));
    for i=1:length(Ipoits)
        distance = sum((D2-repmat(D1(:,i),[1 length(Ipts2)])).^2,1);
        [err(i),cor2(i)] = min(distance);
    end
    sort(err);
end

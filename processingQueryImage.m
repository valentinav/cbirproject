function queryImage = processingQueryImage(imag)
    Options.upright=true;
    Options.tresh=0.0001;
    queryImage.Ipoits = OpenSurf(imag, Options);
    queryImage.D1 = reshape([queryImage.Ipoits.descriptor],64,[]);
    queryImage.normVecGLCM = GLCM(imag);
end
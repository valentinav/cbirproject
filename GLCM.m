function normalizedVec = GLCM(image)
    grayI=rgb2gray(image);
    glcmI=graycomatrix(grayI,'NumLevels',256,'GrayLimits',[],'Offset',[0 1;-1 1;-1 0;-1 -1]);
    stastI=graycoprops(glcmI,{'all'});
    featureVec=[stastI.Contrast stastI.Correlation stastI.Energy stastI.Homogeneity];
    normalizedVec = featureVec/norm(featureVec);
end 
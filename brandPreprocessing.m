function IM = brandPreprocessing()

    functionname='brandPreprocessing.m';
    functiondir=which(functionname);
    functiondir=functiondir(1:end-length(functionname));
    addpath([functiondir '/DATASET'])
    
    path = [functiondir 'DATASET/ROI/'];
    c1_read = dir([path '*.jpg']);

    for i = 1:length(c1_read)
        c1I = c1_read(i).name;
        c1namei=path;

        IM{i}.image = imread(strcat(c1namei,c1I));
        IM{i}.name = c1I;
    end

    Options.upright=true;
    Options.tresh=0.0001;

    for i = 1:length(c1_read)
        IM{i}.Ipts2 = OpenSurf(IM{i}.image,Options);
        IM{i}.D2 = reshape([IM{i}.Ipts2.descriptor],64,[]);
        IM{i}.normaVecGLCM = GLCM(IM{i}.image);
    end
end
  

function varargout = cbirGUI(varargin)
% CBIRGUI MATLAB code for cbirGUI.fig
%      CBIRGUI, by itself, creates a new CBIRGUI or raises the existing
%      singleton*.
%
%      H = CBIRGUI returns the handle to a new CBIRGUI or the handle to
%      the existing singleton*.
%
%      CBIRGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CBIRGUI.M with the given input arguments.
%
%      CBIRGUI('Property','Value',...) creates a new CBIRGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before cbirGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to cbirGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help cbirGUI

% Last Modified by GUIDE v2.5 27-Oct-2020 11:15:08

% Begin initialization code - DO NOT EDIT

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @cbirGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @cbirGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before cbirGUI is made visible.
function cbirGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to cbirGUI (see VARARGIN)

% Choose default command line output for cbirGUI
handles.output = hObject;

%información de las imag en el dataset
handles.IM = brandPreprocessing();

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes cbirGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = cbirGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hold off
% para cargar la imagen se abre la ventana para cargar el fichero
clc
filter = ('*.*');
[file_image, ruta]=uigetfile(filter);
img = imread(strcat(ruta,file_image));
figure(1), imshow(img);
%Recorte de la imagen
[rec, pos] = imcrop(img);
handles.img=rec;
guidata(hObject, handles);
%Mostrar el recorte de la imagen en la interfaz
imshow(rec, 'Parent', handles.axes1);
%cerrar la imagen original
close(figure(1));

%preprocesa la imágen
handles.queryImage = processingQueryImage(handles.img);

guidata(hObject,handles);




% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc
dataset = SimilarityMetrics(handles.queryImage, handles.IM);
for i=1:length(dataset)
[values(i),index(i)] = sort([dataset{i}.minDistance],'ascend');
end
[values, index] = sort(values);


path = 'DATASET/ORIGINALES/';

arrayD{1} = imread(strcat(path, handles.IM{index(1)}.name));
handles.img=arrayD{1};
imageSize = size(arrayD{1});
numrows = imageSize(1);
numcols = imageSize(2);
for i = 2:6
   arrayD{i} = imread(strcat(path, handles.IM{index(i)}.name));
   arrayD{i} = imresize(arrayD{i},[numrows numcols]);
end

for i=1:6
    distanc{i}=dataset{index(i)}.minDistance;
end
montage(arrayD, 'Parent', handles.axes2);
%guidata(hObject,handles);
